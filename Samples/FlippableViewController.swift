//
//  FlippableViewController.swift
//  Samples
//
//  Created by kaveh elierdy on 8/6/15.
//
//

import UIKit

class FlippableViewController: UIViewController {

    var isFlipped: Bool = false
    
    @IBOutlet var containerView: UIView!
    @IBOutlet weak var rearView: UIView!
    @IBOutlet weak var frontView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let tapGesture = UITapGestureRecognizer(target: self, action: "handleTap:")
        containerView.addGestureRecognizer(tapGesture)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func handleTap(recognizer: UITapGestureRecognizer) {
        if isFlipped {
            UIView.transitionFromView(rearView,
                toView: frontView,
                duration: 1.0,
                options: [UIViewAnimationOptions.TransitionFlipFromLeft, UIViewAnimationOptions.ShowHideTransitionViews],
                completion:nil)
        } else {
            UIView.transitionFromView(frontView,
                toView: rearView,
                duration: 1.0,
                options: [UIViewAnimationOptions.TransitionFlipFromRight, UIViewAnimationOptions.ShowHideTransitionViews],
                completion: nil)
        }
        isFlipped = !isFlipped
    }

}
