//
//  DraggableViewController.swift
//  Samples
//
//  Created by kaveh elierdy on 8/6/15.
//
//

import UIKit

class DraggableViewController: UIViewController {
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var draggableView: UIView!
    
    let center = CGPoint(x: UIScreen.mainScreen().bounds.width/2, y: UIScreen.mainScreen().bounds.height/2)
    var startLocation: CGPoint?
    var directionDown: Bool = false
    let topMargin: CGFloat = 80.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let panGesture = UIPanGestureRecognizer(target: self, action: "handlePan:")
        draggableView.addGestureRecognizer(panGesture)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
// MARK: - UIPanGestureRecognizer
    
    func handlePan(recognizer: UIPanGestureRecognizer) {
        
        if recognizer.state == UIGestureRecognizerState.Began {
            startLocation = recognizer.locationInView(recognizer.view)
        } else if recognizer.state == UIGestureRecognizerState.Changed {
            let currentLocation = recognizer.locationInView(recognizer.view)
            let translation = recognizer.translationInView(recognizer.view!)
            if currentLocation.y > startLocation!.y {
                directionDown = true
                activityIndicator.startAnimating()
                recognizer.view!.center = CGPointMake(recognizer.view!.center.x, recognizer.view!.center.y + translation.y)
                recognizer.setTranslation(CGPoint(x: 0.0, y: 0.0), inView: recognizer.view)
            }
        } else if recognizer.state == UIGestureRecognizerState.Ended {
            if directionDown {
//                let endLocation = recognizer.locationInView(recognizer.view)
                let animation = CABasicAnimation(keyPath: "position.y")
                animation.fromValue = draggableView.frame.height
                animation.toValue = self.center.y + topMargin
                animation.duration = 0.3
                recognizer.view!.layer.addAnimation(animation, forKey: "moveByYBelowActivityIndicator")
                recognizer.view!.center = CGPoint(x: center.x, y: self.center.y + topMargin)
            }
        }
    }
    
    // Helper: Call this methode when loading is done
    
    func finishedLoading() {
        let animation = CABasicAnimation(keyPath: "position.y")
        animation.fromValue = draggableView.frame.height
        animation.toValue = self.center.y
        animation.duration = 0.3
        draggableView.layer.addAnimation(animation, forKey: "moveByYToTheTop")
        draggableView.center = CGPoint(x: center.x, y: self.center.y)
        activityIndicator.stopAnimating()
    }
    
}
