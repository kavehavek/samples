//
//  ShakableViewController.swift
//  Samples
//
//  Created by kaveh elierdy on 8/6/15.
//
//

import UIKit

class ShakableViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var textField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        shake(textField.layer)
        return true
    }
    
    // Multi-frame shake animation for password textfield
    func shake(layer: CALayer) {
        let animation = CAKeyframeAnimation(keyPath: "position.x")
        animation.values = [0, 10, -10, 10, 0]
        animation.keyTimes = [0, 1/6.0, 3/6.0, 5/6.0, 1]
        animation.additive = true
        layer.addAnimation(animation, forKey: "shake")
    }
}
